package org.example

// data class
// TODO: TASK #14: Create data class Product with properties:
//  * name of type String
//  * price of type Double
//  * quantity of type Int
data class Product(/* TODO: Your code */) {

    // TODO: TASK #15: **Within** Product data class, create function totalValue which will calculate and return total value
    //  of products and will return it as Double.
    fun totalValue(): Double {
        // TODO: Implement this function
    }

    // TODO: TASK #16: Duplicate/rewrite (from scratch) totalValue as class property with overriden getter.
    // Implementation:
}

// sealed class
// TODO: TASK #17: Create a sealed named PaymentStatus to represent status of payment: pending, success, failed, refunded
//  Failed transaction has property "errorCode" of type Int, which will be used later.
//  Refunded transaction has property "amount" of type Double, which will be used later.
sealed class PaymentStatus {
    // Implementation:

    // TODO: Write a function resolvePayment which will return different string for different status. Don't forget about properties.
    // Pending     "Your payment is pending. Please be patient."
    // Success     "Payment successful!"
    // Failed      "Uh oh. Something went wrong. Error code: $errorCode"
    // Refunded    "Money charged back"
    fun resolvePayment(): String {
        TODO("Implement this function")
    }
}

// enum class
// TODO: TASK #18: Create (from scratch) enum class Currency with following values: CZK, EUR, USD, JPY, CAD. Each currency should
//  have a corresponding exchangeRate to CZK - exchangeRateToCZK (you can make it up or google it).

// TODO: TASK #19: Write function convertToCZK that takes amount of type Double and converts it to CZK based on the exchange rate.

// object
// TODO: TASK #20: Create (from scratch) singleton class named Singleton

// TODO: TASK #21: Create (from scratch) class Logger with static property TAG of type String with value "MainActivity"

interface Listener {
    fun onListen()
    fun onDetached()
}

// TODO: TASK #22: Implement interface Listener. Interface functions will be implemented:
//  * onListen      - prints "Listening..."
//  * onDetached    - prints "Detaching, not listening anymore."
class App {
    val listener: Listener = TODO()

    init {
        listener.onListen()
        listener.onDetached()
    }
}