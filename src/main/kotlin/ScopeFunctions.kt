package org.example

// TODO: TASK #7: Implement function printNonNullValue which will return Any object.
//  If object is not null, **return it**
//  If object is null, **print** "Thing is null".
//  ! You cannot use if statement !
//  Answer: Why does it work?
fun getNonNullValue(thing: Any?): Any {
    TODO("Implement this function")
}

// TODO: TASK #8: Implement function getNullableLength which will return length of list.
//  If list is null, return -1.
//  You cannot use if statement.
//  You cannot use let statement.
//  Implement as one line return.
fun getNullableLength(numbers: List<Int>?): Int {
    TODO("Implement this function")
}

// TODO: TASK #9: Implement makeAdult function which updates age to 18. Use with scope function.
data class Person(var name: String, var age: Int)

fun makeAdult(person: Person): Person {
    TODO("Implement this function")
}

// TODO: TASK #10: Implement greetings function, which will create StringBuilder and will append two or more strings which will
//  result in output "Hello World"
fun greetings(): StringBuilder {
    TODO("Implement this function")
}

private fun notify(logMessage: String) = println("Notified: $logMessage")

// TODO: TASK #11: Implement function logMessage that takes message of type String and returns it together with current time.
//  To get current time, you can use System.currentTimeMillis(). As side effect, pass message to notify() function.
//  Implement function as one line return.
// Example output format
// [1235123]: Provided message
fun logMessage(message: String): String {
    TODO("Implement this function")
}


