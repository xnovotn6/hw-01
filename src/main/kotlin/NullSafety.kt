package org.example

class Client(val personalInfo: PersonalInfo?)

class PersonalInfo(val email: String?)

interface Mailer {
    fun sendMessage(email: String, message: String)
}

// TODO: TASK #5: Implement sendMessageToClient function, so mailer is able to sent message to client.
fun sendMessageToClient(client: Client?, message:String?, mailer: Mailer) {
    TODO("Implement this function")
}


// TODO: TASK #6: Make doubleBang function throw NPE (NullPointerException).
fun doubleBang() {
    TODO("Implement this function")
}
