package org.example

// Basic functions
// TODO: TASK #1: Write (from scratch) function sum, which will return sum of two numbers
// Implementation:

// Lambda functions
// TODO: TASK #2: Implement onlyPositive function which will return only positive numbers from a given list. Use the filter() function.
fun onlyPositive(): List<Int> {
    val list = listOf(-2, -1, 0, 1, 2)
    TODO("Implement this function")
}

// Higher-order functions
fun calculate(): Int {
    val list = listOf(1, 2, 3)
    return applyNumericOperation(list, ::sum)
}

// TODO: TASK #3: Write operation parameter type of a a higher-order function applyNumericOperation which
//  takes a list of Integers and a function (operation) as arguments and applies the operation on the list.
fun applyNumericOperation(list: List<Int>, operation: /* TODO: Your code */): Int {
    return list.fold(0, operation)
}

// Nothing
// TODO: TASK #4: Implement emptiness function.
fun emptiness(): Nothing {
    TODO("Implement this function")
}

