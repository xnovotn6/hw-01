import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

import org.example.*


class Extensions {
    @Test
    fun testCapitalizeWords() {
        val text = "hello world"
        assertEquals("Hello World", text.capitalizeWords())
    }

    @Test
    fun testCapitalizeWords_emptyString() {
        val text = ""
        assertEquals("", text.capitalizeWords())
    }

    @Test
    fun testVowelCount() {
        val text = "hello world"
        assertEquals(3, text.vowelCount)
    }

    @Test
    fun testVowelCount_noVowels() {
        val text = "bcdfghjkl"
        assertEquals(0, text.vowelCount)
    }

    @Test
    fun testVowelCount_allVowels() {
        val text = "aeiou"
        assertEquals(5, text.vowelCount)
    }
}