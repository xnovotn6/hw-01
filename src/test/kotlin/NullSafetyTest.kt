import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.junit.jupiter.api.assertThrows

import org.example.*


class NullSafetyTest {

    @Test
    fun testSendMessageToClient_withNonNullClientAndMessage() {
        // Arrange
        val client = Client(PersonalInfo("test@example.com"))
        val mailerMock = mock(Mailer::class.java)

        // Act
        sendMessageToClient(client, "Test message", mailerMock)

        // Assert
        verify(mailerMock, times(1)).sendMessage("test@example.com", "Test message")
    }

    @Test
    fun testSendMessageToClient_withNullClient() {
        // Arrange
        val mailerMock = mock(Mailer::class.java)

        // Act
        sendMessageToClient(null, "Test message", mailerMock)

        // Assert
        verify(mailerMock, never()).sendMessage(anyString(), anyString())
    }

    @Test
    fun testSendMessageToClient_withNullMessage() {
        // Arrange
        val client = Client(PersonalInfo("test@example.com"))
        val mailerMock = mock(Mailer::class.java)

        // Act
        sendMessageToClient(client, null, mailerMock)

        // Assert
        verify(mailerMock, never()).sendMessage(anyString(), anyString())
    }

    @Test
    fun testSendMessageToClient_withNullClientAndNullMessage() {
        // Arrange
        val mailerMock = mock(Mailer::class.java)

        // Act
        sendMessageToClient(null, null, mailerMock)

        // Assert
        verify(mailerMock, never()).sendMessage(anyString(), anyString())
    }

    @Test
    fun testDoubleBangThrowsNPE() {
        assertThrows<NullPointerException> {
            doubleBang()
        }
    }
}