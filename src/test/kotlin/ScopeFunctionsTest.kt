import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

import org.example.*

class ScopeFunctionsTest {

    @Test
    fun testGetNonNullValue_withNonNullObject() {
        // Arrange
        val obj = "Non-null object"

        // Act
        val result = getNonNullValue(obj)

        // Assert
        assertEquals(obj, result)
    }

    @Test
    fun testGetNonNullValue_withNullObject() {
        // Arrange
        val obj: Any? = null

        // Act
        val result = getNonNullValue(obj)

        // Assert
        assertEquals(Unit, result)
    }

    @Test
    fun testGetNullableLength_withNonNullList() {
        // Arrange
        val list = listOf(1, 2, 3)

        // Act
        val result = getNullableLength(list)

        // Assert
        assertEquals(3, result)
    }

    @Test
    fun testGetNullableLength_withNullList() {
        // Arrange
        val list: List<Int>? = null

        // Act
        val result = getNullableLength(list)

        // Assert
        assertEquals(-1, result)
    }

    @Test
    fun testMakeAdult() {
        // Arrange
        val person = Person("Alice", 25)

        // Act
        val result = makeAdult(person)

        // Assert
        assertEquals(18, result.age)
    }

    @Test
    fun testGreetings() {
        // Act
        val result = greetings().toString()

        // Assert
        assertEquals("Hello World", result)
    }
}