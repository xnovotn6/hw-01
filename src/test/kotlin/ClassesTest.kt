import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

import org.example.*

class ProductTest {

    @Test
    fun testTotalValueFunction() {
        val product = Product("Test Product", 10.0, 5)
        assertEquals(50.0, product.totalValue())
    }

    @Test
    fun testTotalValueProperty() {
        val product = Product("Test Product", 10.0, 5)
        assertEquals(50.0, product.totalValue)
    }
}

class PaymentStatusTest {

    @Test
    fun testToStringForPending() {
        val paymentStatus = PaymentStatus.Pending
        assertEquals("Your payment is pending. Please be patient.", paymentStatus.resolvePayment())
    }

    @Test
    fun testToStringForSuccess() {
        val paymentStatus = PaymentStatus.Success
        assertEquals("Payment successful!", paymentStatus.resolvePayment())
    }

    @Test
    fun testToStringForFailed() {
        val errorCode = 404
        val paymentStatus = PaymentStatus.Failed(errorCode)
        assertEquals("Uh oh. Something went wrong. Error code: $errorCode", paymentStatus.resolvePayment())
    }

    @Test
    fun testToStringForRefunded() {
        val amount = 4.21
        val paymentStatus = PaymentStatus.Refunded(amount)
        assertEquals("Money charged back: $$amount", paymentStatus.resolvePayment())
    }
}

class CurrencyTest {

    @Test
    fun testConvertToCZK() {
        val amount = 100.0
        assertEquals(amount, Currency.CZK.convertToCZK(amount))
        assertEquals(amount * Currency.EUR.exchangeRateToCZK, Currency.EUR.convertToCZK(amount))
        assertEquals(amount * Currency.USD.exchangeRateToCZK, Currency.USD.convertToCZK(amount))
        assertEquals(amount * Currency.CAD.exchangeRateToCZK, Currency.CAD.convertToCZK(amount))
        assertEquals(amount * Currency.JPY.exchangeRateToCZK, Currency.JPY.convertToCZK(amount))
    }
}

class SingletonTest {

    @Test
    fun testSingletonInstance() {
        val instance1 = Singleton
        val instance2 = Singleton
        assertEquals(instance1, instance2)
    }
}

class LoggerTest {

    @Test
    fun testTAGValue() {
        assertEquals("MainActivity", Logger.TAG)
    }
}

class AppTest {

    private val app = App()

    @Test
    fun testListenerOnListen() {
        assertEquals("Listening...", getOutput { app.listener.onListen() })
    }

    @Test
    fun testListenerOnDetached() {
        assertEquals("Detaching, not listening anymore.", getOutput { app.listener.onDetached() })
    }

    private fun getOutput(block: () -> Unit): String {
        val consoleOutput = System.out
        val outputStreamCaptor = java.io.ByteArrayOutputStream()
        System.setOut(java.io.PrintStream(outputStreamCaptor))
        block()
        System.setOut(consoleOutput)
        return outputStreamCaptor.toString().trim()
    }
}