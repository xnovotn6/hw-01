import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows

import org.example.*

class FunctionsTest {

    @Test
    fun testOnlyPositive() {
        val result = onlyPositive()
        assertEquals(listOf(0, 1, 2), result)
    }

    @Test
    fun testCalculate() {
        val result = calculate()
        assertEquals(6, result)
    }

    @Test
    fun testApplyNumericOperation() {
        val list = listOf(1, 2, 3)
        val result = applyNumericOperation(list, ::sum)
        assertEquals(6, result)
    }

    @Test
    fun testEmptiness() {
        assertThrows<Exception> {
            emptiness()
        }
    }

    @Test
    fun testApplyNumericOperationWithDifferentOperations() {
        val list = listOf(1, 2, 3, 4, 5)

        val result1 = applyNumericOperation(list) { x, y -> x + y } // sum
        assertEquals(15, result1)

        val result2 = applyNumericOperation(list) { x, y -> x * y } // multiplication
        // Folds starts with 0, hence everything will be always 0
        assertEquals(0, result2)

        val result3 = applyNumericOperation(list) { x, y -> x - y } // subtraction
        assertEquals(-15, result3)
    }
}